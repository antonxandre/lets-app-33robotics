import 'package:flutter/material.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';

class GridGrupos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
        shrinkWrap: true,
        crossAxisCount: 2,
        children: List.generate(opcoes.length, (index) {
          return Center(
            child: OpcaoCard(opcao: opcoes[index]),
          );
        }));
  }
}

class Opcao {
  const Opcao({this.titulo, this.icon});
  final String titulo;
  final IconData icon;
}

const List<Opcao> opcoes = const <Opcao>[
  const Opcao(titulo: 'Cartão de visita 1', icon: Icons.credit_card),
  const Opcao(titulo: 'Cartão de visita 2', icon: Icons.credit_card),
  const Opcao(titulo: 'Cartão de visita 3', icon: Icons.credit_card),
  const Opcao(titulo: 'Cartão de visita 4', icon: Icons.credit_card),
  const Opcao(titulo: 'Cartão de visita 5', icon: Icons.credit_card),
  const Opcao(titulo: 'Cartão de visita 6', icon: Icons.credit_card),
];

class OpcaoCard extends StatelessWidget {
  const OpcaoCard({Key key, this.opcao}) : super(key: key);
  final Opcao opcao;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
          decoration: BoxDecoration(
            border: Border.all(width: 1.0, color: ColorsApp.colorApp),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Center(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    opcao.icon,
                    size: 80.0,
                    color: ColorsApp.colorApp,
                  ),
                  Divider(
                    color: ColorsApp.colorApp,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    opcao.titulo,
                    style: TextStyle(fontSize: 16, color: ColorsApp.colorApp),
                  ),
                ]),
          )),
    );
  }
}
