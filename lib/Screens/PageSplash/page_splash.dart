import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';

class PageSplash extends StatefulWidget {
  @override
  PageSplashState createState() => PageSplashState();
}

class PageSplashState extends State<PageSplash> {
  void navigationToNextPage() {
    Navigator.pushReplacementNamed(context, '/HomePage');
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {
    super.initState();
    startSplashScreenTimer();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Container(
        color: Colors.yellow[500], child: new Image.asset('assets/lets.png'));
  }
}
