import 'package:projeto_soma/DadosUsuario/dados_usuario.dart';
import 'package:projeto_soma/Screens/PageCadastroUser/cadastro_services.dart';
import 'package:projeto_soma/models/usuario.dart';
import 'package:rxdart/rxdart.dart';

class CadastroBloc {
  final _nomeUsuario = BehaviorSubject<String>();
  final _cpfcnpj = BehaviorSubject<String>();
  final _endereco = BehaviorSubject<String>();
  final _numero = BehaviorSubject<String>();
  final _complemento = BehaviorSubject<String>();
  final _cidade = BehaviorSubject<String>();
  final _estado = BehaviorSubject<String>();
  final _telefone = BehaviorSubject<String>();
  final _email = BehaviorSubject<String>();

  Stream<String> get nomeUsuario => _nomeUsuario.stream;
  Stream<String> get cpfcnpj => _cpfcnpj.stream;
  Stream<String> get endereco => _endereco.stream;
  Stream<String> get numero => _numero.stream;
  Stream<String> get complemento => _complemento.stream;
  Stream<String> get cidade => _cidade.stream;
  Stream<String> get estado => _estado.stream;
  Stream<String> get telefone => _telefone.stream;
  Stream<String> get email => _email.stream;

  Function(String) get changeNomeusuario => _nomeUsuario.sink.add;
  Function(String) get changeCpfCnpj => _cpfcnpj.sink.add;
  Function(String) get changeEndereco => _endereco.sink.add;
  Function(String) get changeNumero => _numero.sink.add;
  Function(String) get changeComplemento => _complemento.sink.add;
  Function(String) get changeCidade => _cidade.sink.add;
  Function(String) get changeEstado => _estado.sink.add;
  Function(String) get changeTelefone => _telefone.sink.add;
  Function(String) get changeEmail => _email.sink.add;

  CadastroServices _cadastroServices = CadastroServices();

  //Verifica numero se existe cadastro na base;
  void submit() async {
    Usuario usuario = new Usuario();
    usuario.nome = _nomeUsuario.value;
    usuario.cpfcnpj = _cpfcnpj.value;
    usuario.endereco = _endereco.value;
    usuario.numero = _numero.value;
    usuario.complemento = _complemento.value;
    usuario.cidade = _cidade.value;
    usuario.estado = _estado.value;
    usuario.celular = _telefone.value;
    usuario.email = _email.value;
    await _cadastroServices
        .update(DadosUsuario.usario.id, usuario)
        .then((response) {
      try {
        if (response != null) {
          return response;
        }
      } catch (e) {
        return e;
      }
    });
  }

  void dispose() {
    _nomeUsuario.close();
    _cpfcnpj.close();
    _endereco.close();
    _numero.close();
    _complemento.close();
    _cidade.close();
    _estado.close();
    _telefone.close();
    _email.close();
  }
}
