import 'package:projeto_soma/Services/abstract_services.dart';

class PhoneServices extends AbstractService {
  PhoneServices() : super('/auth/info');

  Future<dynamic> checkPhone(String celular) =>
      Session.post('$api?celular=$celular').then((json) {
        print(api + '?celular=$celular');
        return json.length == 0 ? null : fromJson(json);
      });

  @override
  fromJson(json) {
    return json;
  }
}
